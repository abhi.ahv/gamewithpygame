white = (255, 255, 255)
black = (0, 0, 0)
brown = (160, 120, 116)
darkblue = (2, 25, 255)
blue = (0, 192, 255)

style = "THANK YOU KOBE.ttf"

score = "score: "
p1 = "PLAYER1"
p2 = "PLAYER2"
en = "end"
st = "Start"
le = "Level-"
ys = " your score: "
p1w = " Player1 WINS "
p2w = " Player2 WINS "
lgnr = "Lets go to Next Round..."
pcc = "PRESS C TO CONTINUE...."
oobs = " OOPS YOU HIT AN OBSTACLE...."
eyc = "EXCELLENT, YOU COMPLETED..."
pq = "IF YOU WANT TO EXIT PRESS Q"
soun = "audio.mp3"
tim ="  Time: "