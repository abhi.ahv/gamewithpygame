import pygame
import math
import random
import config
pygame.init()
clk = pygame.time.Clock()
ch = 0  # to know which player is playing
status = True
incr1 = 4
incr2 = 4

t1 = 0
t2 = 0

player1X = 380
player1Y = 690
player2Y = 50
player2X = 500

screen = pygame.display.set_mode((1330, 790))  # set the screen (width, height)
pygame.display.set_caption("Ride Safe")  # set the name
scorep1 = 0  # score of player1
scorep2 = 0  # score of player2

font = pygame.font.Font(config.style, 30)  # one of the type of font
ofont = pygame.font.Font(config.style, 72)

player1img = pygame.image.load('hovercraft.png')  # set the players icon

p1Xc = 0  # change in x coordinate of player
p1Yc = 0  # change in y coordinate of player

movobs = []  # create a list to store all the moving obstacles
movobsx = []  # store all the attributes of a obstacle in a list
movobsy = []  # x-coordinate of moving obstacle
movcom = []  # y-coordinate of moving obstacle and tocheck visited of obstacle

movobs.append(pygame.image.load('boat.png'))
movobsx.append(0)
movobsy.append(110)  # movcom is to ensure points wont be allotedwhen you move to and fro
movcom.append(0)  # w.r.to element

movobs.append(pygame.image.load('ocean.png'))
movobsx.append(400)
movobsy.append(110)
movcom.append(0)

movobs.append(pygame.image.load('boat.png'))
movobsx.append(0)
movobsy.append(330)
movcom.append(0)

movobs.append(pygame.image.load('sharki.png'))
movobsx.append(600)
movobsy.append(330)
movcom.append(0)

movobs.append(pygame.image.load('subm.png'))
movobsx.append(0)
movobsy.append(550)
movcom.append(0)

movobs.append(pygame.image.load('ocean.png'))
movobsx.append(900)
movobsy.append(550)
movcom.append(0)

fixobs = []
fixobsx = []
fixobsy = []
fixcom = []

fixobs.append(pygame.image.load('island.png'))
fixobsx.append(180)
fixobsy.append(220)
fixcom.append(0)

fixobs.append(pygame.image.load('island.png'))
fixobsx.append(780)
fixobsy.append(220)
fixcom.append(0)

for i in range(random.randint(4, 8)):
    fixobs.append(pygame.image.load('island.png'))
    fixobsx.append(random.randint(10, 1200))
    fixobsy.append(220)
    fixcom.append(0)

for i in range(random.randint(3, 9)):
    fixobs.append(pygame.image.load('island.png'))
    fixobsx.append(random.randint(120, 1100))  # random used to create obstacles at different positions
    fixobsy.append(440)
    fixcom.append(0)

fixobs.append(pygame.image.load('island.png'))
fixobsx.append(580)
fixobsy.append(440)
fixcom.append(0)

fixobs.append(pygame.image.load('island.png'))
fixobsx.append(1080)
fixobsy.append(440)
fixcom.append(0)


def movobsf(b):                                         # function to draw the moving obstacle at desired positions
    screen.blit(movobs[b], (movobsx[b], movobsy[b]))    # blit renders the display of obstacle


def fixobsf(b):     # function to draw the moving obstacle at desired positions
    screen.blit(fixobs[b], (fixobsx[b], fixobsy[b]))


def playerf(x, y):  # function to draw the player
    screen.blit(player1img, (x, y))


def printscore(sci, incr, t):
    global ch   # to find the player and level
    sc = font.render(config.score + str(sci), True, config.white)
    screen.blit(sc, (5, 5))     # to display the score at the top
    if (ch % 2) == 0:       # ch = even for player 1
        screen.blit(font.render(config.en, True, config.white), (675, 5))
        screen.blit(font.render(config.le + str((incr - 1)) + config.tim + str(t), True, config.white), (975, 5))
        screen.blit(font.render(config.p1, True, config.white), (320, 5))
        screen.blit(font.render(config.st, True, config.white), (720, 754))
    if (ch % 2) == 1:       # ch = odd for player2
        screen.blit(font.render(config.st, True, config.white), (675, 5))
        screen.blit(font.render(config.le + str((incr - 1)) + config.tim + str(t), True, config.white), (975, 5))
        screen.blit(font.render(config.p2, True, config.white), (320, 5))
        screen.blit(font.render(config.en, True, config.white), (720, 754))


def collide(sci):
    global p1Xc, p1Yc, player1X, player1Y, player2X, player2Y, ch, scorep1, scorep2, status, t1, t2
    player1X = 380
    player1Y = 690
    player2Y = 50
    player2X = 500
    p1Xc = 0
    p1Yc = 0
    pap = 1
    while pap:
        screen.fill(config.black)
        if (ch % 2) == 0:
            screen.blit(ofont.render(config.p1 + config.ys + str(sci), True, config.darkblue), (200, 200))
            # to display the score of every player after he completes the round
        else:
            screen.blit(ofont.render(config.p2 + config.ys + str(sci), True, config.darkblue), (200, 200))
        screen.blit(ofont.render(config.oobs, True, config.darkblue), (200, 300))
        screen.blit(ofont.render(config.pcc, True, config.darkblue), (200, 400))
        if (ch % 2) == 1:
            if scorep1 > scorep2:
                screen.blit(ofont.render(config.p1w, True, config.darkblue), (200, 500))
            if scorep2 > scorep1:
                screen.blit(ofont.render(config.p2w, True, config.darkblue), (200, 500))
            if scorep1 == scorep2:
                if t1 > t2:
                    screen.blit(ofont.render(config.p2w, True, config.darkblue), (200, 500))
                if t1 < t2:
                    screen.blit(ofont.render(config.p1w, True, config.darkblue), (200, 500))
                if t1 == t2:
                    screen.blit(ofont.render(config.p1w + config.p2w, True, config.darkblue), (200, 500))
            screen.blit(ofont.render(config.lgnr, True, config.darkblue), (200, 600))
            screen.blit(ofont.render(config.pq, True, config.darkblue), (200, 700))
        pygame.display.update()
        for k in range(len(fixobs)):
            fixcom[k] = 0
        for k in range(len(movobs)):
            movcom[k] = 0
        for evene in pygame.event.get():
            if evene.type == pygame.KEYUP:
                if evene.key == pygame.K_c:
                    ch += 1
                    pap = 0
                if evene.key == pygame.K_q:
                    status = False  # to quit the game loop
                    pap = 0
    if (ch % 2) == 0:
        scorep1 = 0
        scorep2 = 0
        t1 = 0
        t2 = 0


def win(sci):
    global p1Yc, p1Xc, player1X, player1Y, player2X, player2Y, incr1, incr2, status, ch, scorep1, scorep2, t1, t2
    p1Xc = 0            # when a new player starts all attributes must be zero.
    player1X = 380      # when a new round starts scores must be zero
    player1Y = 690
    player2Y = 50
    player2X = 500
    p1Yc = 0
    if (ch % 2) == 1:
        incr2 += 2
    if (ch % 2) == 0:
        incr1 += 2
    pap = 1
    while pap:
        screen.fill(config.black)
        if (ch % 2) == 0:
            screen.blit(ofont.render(config.p1 + config.ys + str(sci), True, config.darkblue), (200, 200))
        else:
            screen.blit(ofont.render(config.p2 + config.ys + str(sci), True, config.darkblue), (200, 200))
        screen.blit(ofont.render(config.eyc, True, config.darkblue), (200, 300))
        screen.blit(ofont.render(config.pcc, True, config.darkblue), (200, 400))

        if (ch % 2) == 1:
            if scorep1 > scorep2:
                screen.blit(ofont.render(config.p1w, True, config.darkblue), (200, 500))
            if scorep2 > scorep1:
                screen.blit(ofont.render(config.p2w, True, config.darkblue), (200, 500))
            if scorep1 == scorep2:
                if t1 > t2:
                    screen.blit(ofont.render(config.p2w, True, config.darkblue), (200, 500))
                if t1 < t2:
                    screen.blit(ofont.render(config.p1w, True, config.darkblue), (200, 500))
                if t1 == t2:
                    screen.blit(ofont.render(config.p1w + config.p2w, True, config.darkblue), (200, 500))
            screen.blit(ofont.render(config.lgnr, True, config.darkblue), (200, 600))
            screen.blit(ofont.render(config.pq, True, config.darkblue), (200, 700))
        pygame.display.update()
        for k in range(len(fixobs)):
            fixcom[k] = 0
        for k in range(len(movobs)):
            movcom[k] = 0
        for vent in pygame.event.get():
            if vent.type == pygame.KEYDOWN:
                if vent.key == pygame.K_c:
                    ch += 1
                    pap = 0
                if vent.key == pygame.K_q:
                    status = False
                    pap = 0

    if (ch % 2) == 0:
        scorep1 = 0
        scorep2 = 0
        t1 = 0
        t2 = 0


pygame.mixer.music.load(config.soun)
pygame.mixer.music.play(-1)
# game loop


while status:
    screen.fill(config.blue)    # fill the display window with color
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 110, 1370, 50))   # draw a rectangle with different colors
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 220, 1370, 50))   # inputs given are height,weight,breadth
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 330, 1370, 50))   # length
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 440, 1370, 50))
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 550, 1370, 50))
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 0, 1370, 36))
    pygame.draw.rect(screen, config.brown, pygame.Rect(0, 755, 1380, 36))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:   # to close the game
            status = False
        if event.type == pygame.KEYDOWN:    # giving movement to keys
            if event.key == pygame.K_LEFT:
                p1Xc -= 1.8
            if event.key == pygame.K_RIGHT:
                p1Xc += 1.8
            if event.key == pygame.K_DOWN:
                p1Yc += 1.8
            if event.key == pygame.K_UP:
                p1Yc -= 1.8
        if event.type == pygame.KEYUP:
            p1Yc = 0
            p1Xc = 0

    if (ch % 2) == 0:
        t1 = t1 + 1
        player1X += p1Xc    # update the player x-coordinate
        player1Y += p1Yc    # update the plater y-coordinate
        if player1X <= 5:       # set the boundaries such that object dont cross this
            player1X = 5
        if player1X >= 1260:
            player1X = 1260
        if player1Y >= 735:
            player1Y = 735
        if player1Y <= 10:
            player1Y = 10

        for i in range(len(movobs)):
            movobsx[i] += incr1         # giving movement to moving obstacle
            if movobsx[i] >= 1325:
                movobsx[i] = 0
            movobsf(i)
            dist = math.sqrt((math.pow(player1X - movobsx[i], 2)) + (math.pow(player1Y - movobsy[i], 2)))
            if dist <= 34:
                collide(scorep1)
        for i in range(len(fixobs)):
            fixobsf(i)
            dist = math.sqrt((math.pow(player1X - fixobsx[i], 2)) + (math.pow(player1Y - fixobsy[i], 2)))
            if dist <= 34:
                collide(scorep1)

        for j in range(len(movcom)):
            if movcom[j] == 0:
                if (player1Y + 32) < movobsy[j]:
                    movcom[j] += 1
                    scorep1 += 10 * ((incr1 // 2) - 1)

        for j in range(len(fixcom)):
            if fixcom[j] == 0:
                if (player1Y + 32) < fixobsy[j]:
                    fixcom[j] += 1
                    scorep1 += 5

        if player1Y <= 10:
            win(scorep1)
        playerf(player1X, player1Y)
        printscore(scorep1, (incr1 // 2), int(t1 / 60))  # to display live time,score and the player playing
        pygame.display.update()    # update the screen
        clk.tick(60)
    if (ch % 2) == 1:
        t2 = t2 + 1
        player2X += p1Xc
        player2Y += p1Yc
        if player2X <= 5:
            player2X = 5
        if player2X >= 1260:
            player2X = 1260
        if player2Y >= 735:
            player2Y = 735
        if player2Y <= 10:
            player2Y = 10
        playerf(player2X, player2Y)
        for i in range(len(movobs)):
            movobsx[i] += incr2
            if movobsx[i] >= 1325:
                movobsx[i] = 0
            movobsf(i)
            dist = math.sqrt((math.pow(player2X - movobsx[i], 2)) + (math.pow(player2Y - movobsy[i], 2)))
            if dist <= 34:  # condition for collision
                collide(scorep2)
        for i in range(len(fixobs)):
            fixobsf(i)
            dist = math.sqrt((math.pow(player2X - fixobsx[i], 2)) + (math.pow(player2Y - fixobsy[i], 2)))
            if dist <= 34:  # condition for collision
                collide(scorep2)
        for j in range(len(movcom)):
            if movcom[j] == 0:
                if (player2Y - 32) > movobsy[j]:
                    movcom[j] += 1
                    scorep2 += 10 * ((incr2 // 2) - 1)  # as level increases points alloted when moving obstacle is
        # crossed increases
        for j in range(len(fixcom)):
            if fixcom[j] == 0:
                if (player2Y - 32) > fixobsy[j]:
                    fixcom[j] += 1
                    scorep2 += 5
        if player2Y >= 700:
            win(scorep2)
        printscore(scorep2, (incr2 // 2), int(t2 / 60))  # to display live time,score and the player playing
        pygame.display.update()
        clk.tick(60)
